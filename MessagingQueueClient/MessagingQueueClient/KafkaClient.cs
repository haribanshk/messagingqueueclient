﻿using Confluent.Kafka;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace MessagingQueueClient
{
    public class KafkaClient : IMessagingQueueInterface
    {
        public bool PublishToMQ(ConnectionParameter parameter, string RoutingKey, string Message)
        {
            bool ret = false;
            try
            {
                var kafkaclientconfig = new ClientConfig();
                kafkaclientconfig.BootstrapServers = parameter.HostName.ToString() +":"+ parameter.PortNo.ToString() ;
                kafkaclientconfig.ClientId = Dns.GetHostName();

                var Kafkapubconfig = new ProducerConfig(kafkaclientconfig);

                using (var producer = new ProducerBuilder<Null, string>(Kafkapubconfig).Build())
                {
                    var response = producer.ProduceAsync(RoutingKey, new Message<Null, string> { Value = Message }).GetAwaiter().GetResult();
                    // Console.WriteLine(response);
                }
                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
                throw ex;
            }

            return ret;
        }
    }
}
