﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace MessagingQueueClient
{
    public interface IMessagingQueueInterface
    {
        //bool PublishMessages(string Messages);

        bool PublishToMQ(ConnectionParameter parameter, string RoutingKey, string Message);

    }

    public class MessagingQueueHandler
    {
        public static IMessagingQueueInterface GetInstance(MQClient Value)
        {
            string strProviderType = string.Format("MessagingQueueClient.{0}", Value.ToString("G"));
            Assembly assembly = Assembly.Load("MessagingQueueClient"); // in the same assembly!
            Type type = assembly.GetType(strProviderType); // full name - i.e. with namespace (perhaps concatenate)
            IMessagingQueueInterface objInterface = (IMessagingQueueInterface)Activator.CreateInstance(type);
            return objInterface;
        }
    }

    public enum MQClient
    {
        RabbitMQClient = 1,
        KafkaClient=2
    }

    public class ConnectionParameter
    {
        public string HostName { get; set; }
        public int PortNo { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }
    }

}
