﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace MessagingQueueClient
{
   public class RabbitMQClient : IMessagingQueueInterface
    {

        
        public bool PublishToMQ(ConnectionParameter parameter,string RoutingKey,string Message)
        {
            bool ret = false;
            try
            {
                string ExchangeName = "main.direct.exchange";
                var factory = new ConnectionFactory() { HostName = parameter.HostName, UserName = parameter.UserName, Password = parameter.Password, Port = parameter.PortNo };
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(exchange: ExchangeName, type: "direct");

                    string message = Message;
                    var body = Encoding.UTF8.GetBytes(message);

                    var properties = channel.CreateBasicProperties();
                    properties.DeliveryMode = 2; // persistent

                    channel.BasicPublish(exchange: ExchangeName,
                                         routingKey: RoutingKey,
                                         basicProperties: properties,
                                         body: body);
                }
                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
                throw ex;
            }

            return ret;
        }
    }
}
