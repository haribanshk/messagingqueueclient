﻿using MessagingQueueClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MQTestApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string Payload = "Published from DLL Test APP" + DateTime.Now.ToString();

                IMessagingQueueInterface interfaceobj = MessagingQueueHandler.GetInstance(MQClient.RabbitMQClient);
                GetParameterDetail(out ConnectionParameter parameter);
                bool Response = interfaceobj.PublishToMQ(parameter, "ClientInteractionAuditEvent", Payload);
                Console.WriteLine(Response.ToString());
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadLine();
            }
        }

        public static void GetParameterDetail(out ConnectionParameter parameter)
        {
            parameter = new ConnectionParameter();
            parameter.HostName = "192.168.3.81";
            parameter.PortNo = 5672;
            parameter.UserName = "admin";
            parameter.Password = "Kanrad_123";


        }
    }
}
